import java.util.Arrays;

public class EnrisaTest {

	public static void main(String[] args) {
		/*
		 * int jmlGeser = 5; int[] array = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 5, 3, 2,
		 * 1 };
		 * 
		 * System.out.println("Jumlah Geser : " + jmlGeser);
		 * System.out.println("Array Awal : " + Arrays.toString(array));
		 * System.out.println("Array Akhir : " + Arrays.toString(output(jmlGeser,
		 * array)));
		 */
		//biner(24);
		//String s =Integer.toBinaryString(24);
		String s="abcd";
		char[] a=s.toCharArray();
		for(char c:a){
			System.out.println(c);
		}	}

	private static int[] output(int jmlGeser, int[] array) {
		int[] rsl = new int[array.length];
		int arraySizeIndex = array.length - 1;
		for (int i = 0; i <= arraySizeIndex; i++) {
			int currNewIndex = i + jmlGeser;
			if (currNewIndex > arraySizeIndex)
				currNewIndex = currNewIndex - arraySizeIndex - 1;
			rsl[currNewIndex] = array[i];
		}

		return rsl;
	}

	public static void biner(int a) {
		int b[] = new int[5];
		int i = 0;
		for (i = 0; a > 0; i++) {
			b[i] = a % 2;
			a = a / 2;
		}
		int binary[] = new int[i];
		int k = 0;
		for (int j = i - 1; j >= 0; j--) {
			binary[k++] = b[j];
		}
		System.out.println(Arrays.toString(binary));
	}
}
