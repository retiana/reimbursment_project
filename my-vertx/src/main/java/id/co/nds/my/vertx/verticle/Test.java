package id.co.nds.my.vertx.verticle;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import id.co.nds.my.vertx.constants.BPMConstant;
import id.co.nds.my.vertx.constants.VertxConstant;
import id.co.nds.my.vertx.model.BPMStandardResponse;
import id.co.nds.my.vertx.model.ProcessModel;
import id.co.nds.my.vertx.model.StandardRequest;
import id.co.nds.my.vertx.model.StandardResponse;
import id.co.nds.my.vertx.model.TransactionModel;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public class Test extends AbstractVerticle{
	
	@Override
	public void start() {
		vertx.eventBus().consumer(VertxConstant.ADDRESS_SUBMIT_DATA, message -> {
			System.out.println("ini message event bus consumer address submit ============ " + message.body().toString());
			
			final StandardRequest request = Json.decodeValue(message.body().toString(), StandardRequest.class);
			final ProcessModel model = request.getData();
			final StandardResponse resp = new StandardResponse();
			model.setActionName(BPMConstant.ACTION_NAME_ASSIGN_TO_ME);
			vertx.eventBus().send(VertxConstant.ADDRESS_CONNECT_TO_BPM, Json.encode(model), response1 -> {
				if (response1.succeeded()) {
					BPMStandardResponse bpmResp1 = Json.decodeValue(response1.result().body().toString(),
							BPMStandardResponse.class);
					resp.setStatusCode(bpmResp1.getStatus());
					resp.setStatusDesc(bpmResp1.getDesc());
					resp.setUser(request.getUser());
					if (bpmResp1.getStatus().equalsIgnoreCase("200")) {
						model.setActionName(BPMConstant.ACTION_NAME_SET_DATA);
						vertx.eventBus().send(VertxConstant.ADDRESS_CONNECT_TO_BPM, Json.encode(model),
								response2 -> {
									if (response2.succeeded()) {
										BPMStandardResponse bpmResp2 = Json.decodeValue(
												response2.result().body().toString(),
												BPMStandardResponse.class);
										resp.setStatusCode(bpmResp2.getStatus());
										resp.setStatusDesc(bpmResp2.getDesc());
										resp.setUser(request.getUser());

										Date date = Calendar.getInstance().getTime();
										DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
										String strDate = dateFormat.format(date);
										model.getDataModel().setSubmitDate(strDate);
										if (bpmResp2.getStatus().equalsIgnoreCase("200")) {
											model.setActionName(BPMConstant.ACTION_NAME_COMPLETE_TASK);
											vertx.eventBus().send(VertxConstant.ADDRESS_CONNECT_TO_BPM,
													Json.encode(model), response3 -> {
														if (response3.succeeded()) {
															BPMStandardResponse bpmResp3 = Json.decodeValue(
																	response2.result().body().toString(),
																	BPMStandardResponse.class);
															resp.setStatusCode(bpmResp3.getStatus());
															resp.setStatusDesc(bpmResp3.getDesc());
															resp.setUser(request.getUser());
															message.reply(Json.encode(resp));
															vertx.eventBus().send("connect.to.postgres.insert.tx",
																	Json.encode(model), responseLogInput -> {
																		if (responseLogInput.succeeded()) {
																		
																		} else {
																			message.reply(responseLogInput
																					.cause().getMessage());
																		}
																	});
															vertx.eventBus().send("connect.to.couchbase.insert.tx",
																	Json.encode(model), responseLogInput2 -> {
																		if (responseLogInput2.succeeded()) {
																	
																		} else {
																			message.reply(responseLogInput2
																					.cause().getMessage());
																		}
																	});

														} else {
															message.reply(response3.cause().getMessage());
														}
													});
										} else {
											message.reply(Json.encode(resp));
										}
									} else {
										message.reply(response2.cause().getMessage());
									}
								});
					} else {
						message.reply(Json.encode(resp));
					}
				} else {
					message.reply(response1.cause().getMessage());
				}
			});
		});
	}

}
