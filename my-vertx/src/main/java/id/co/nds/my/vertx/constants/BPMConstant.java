package id.co.nds.my.vertx.constants;

public class BPMConstant {
	
	public static final String ACTION_NAME_EXPOSED_PROCESS = "exposedProcess";
	public static final String URL_EXPOSED_PROCESS = "https://ibmbpm-rpa:9443/rest/bpm/wle/v1/exposed/process";
	
	public static final String ACTION_NAME_START_PROCESS = "startProcess";
	public static final String URL_START_PROCESS = "https://ibmbpm-rpa:9443/rest/bpm/wle/v1/process";
	
	public static final String ACTION_NAME_QUERY_AVAILABLE_TASK = "queryAvailableTask";
	public static final String URL_QUERY_AVAILABLE_TASK = "https://ibmbpm-rpa:9443/rest/bpm/wle/v1/tasks/query/IBM.DEFAULTALLTASKSLIST_75";
	
	public static final String ACTION_NAME_ASSIGN_TO_ME = "assignToMe";
	public static final String URL_ASSIGN_TO_ME = "https://ibmbpm-rpa:9443/rest/bpm/wle/v1/task/";
	
	public static final String ACTION_NAME_TASK_DETAIL = "taskDetail";
	public static final String URL_TASK_DETAIL = "https://ibmbpm-rpa:9443/rest/bpm/wle/v1/task/";
	
	public static final String ACTION_NAME_START_TASK = "startTask";
	public static final String URL_START_TASK = "https://ibmbpm-rpa:9443/rest/bpm/wle/v1/task/";
	
	public static final String ACTION_NAME_SET_DATA = "setData";
	public static final String URL_SET_DATA = "https://ibmbpm-rpa:9443/rest/bpm/wle/v1/task/";
	
	public static final String ACTION_NAME_GET_DATA = "getData";
	public static final String URL_GET_DATA = "https://ibmbpm-rpa:9443/rest/bpm/wle/v1/task/";
	
	public static final String ACTION_NAME_COMPLETE_TASK = "completeTask";
	public static final String URL_COMPLETE_TASK = "https://ibmbpm-rpa:9443/rest/bpm/wle/v1/task/";
	
	public static final String ACTION_NAME_FINISH_TASK = "finishTask";
	public static final String URL_FINISH_TASK = "https://ibmbpm-rpa:9443/rest/bpm/wle/v1/task/";

}
