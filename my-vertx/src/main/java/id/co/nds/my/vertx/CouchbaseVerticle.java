package id.co.nds.my.vertx;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.CouchbaseCluster;
import com.couchbase.client.java.document.JsonDocument;
import com.couchbase.client.java.document.json.JsonObject;
import com.couchbase.client.java.env.CouchbaseEnvironment;
import com.couchbase.client.java.env.DefaultCouchbaseEnvironment;
import com.couchbase.client.java.query.N1qlQueryResult;

import id.co.nds.my.vertx.constants.VertxConstant;
import id.co.nds.my.vertx.model.ProcessModel;
import id.co.nds.my.vertx.model.StandardRequest;
import id.co.nds.my.vertx.model.StandardResponse;

import org.apache.log4j.Logger;
import io.vertx.core.*;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;

import com.couchbase.client.java.query.N1qlQuery;
import java.util.concurrent.TimeUnit;

public class CouchbaseVerticle extends AbstractVerticle {
	private static final Logger logger = Logger.getLogger(CouchbaseVerticle.class);
	Bucket bucket;

	@Override
	public void start() {
		vertx.eventBus().consumer(VertxConstant.ADDRESS_INSERT_TX_TO_COUCHBASE, message -> {
			System.out.println("ini message body di couchbase consumer ==================" + message.body().toString());
			final ProcessModel model = Json.decodeValue(message.body().toString(), ProcessModel.class);
			// createSomeData();
			CouchbaseEnvironment env = DefaultCouchbaseEnvironment.builder()
					.socketConnectTimeout((int) TimeUnit.SECONDS.toMillis(10000))

					.connectTimeout(TimeUnit.SECONDS.toMillis(12000)).build();
			Cluster cluster = CouchbaseCluster.create(env, "http://127.0.0.1:8091");
			cluster.authenticate("Administrator", "123456");
			bucket = cluster.openBucket("reimburseData");
			System.out.println("bucket open........");
			JsonObject products = JsonObject.create();
			String keyCouch = model.getProcessId();
			N1qlQuery qlQuery = N1qlQuery.simple("INSERT INTO `reimburseData` (KEY, VALUE) VALUES ('" + keyCouch + "',"
					+ Json.decodeValue(message.body().toString()) + ")");
			System.out.println(qlQuery);
			N1qlQueryResult n1qlQueryResult = bucket.query(qlQuery);
			System.out.println(n1qlQueryResult);
			message.reply("BERHASIL insert ALL!!! \n " + n1qlQueryResult.allRows());
		});

		vertx.eventBus().consumer(VertxConstant.ADDRESS_UPDATE_TX_TO_COUCHBASE, message -> {
			System.out.println(
					"ini message body update couchbase consumer ==================" + message.body().toString());
			final ProcessModel model = Json.decodeValue(message.body().toString(), ProcessModel.class);
			// createSomeData();
			CouchbaseEnvironment env = DefaultCouchbaseEnvironment.builder()
					.socketConnectTimeout((int) TimeUnit.SECONDS.toMillis(10000))

					.connectTimeout(TimeUnit.SECONDS.toMillis(12000)).build();
			Cluster cluster = CouchbaseCluster.create(env, "http://127.0.0.1:8091");
			cluster.authenticate("Administrator", "123456");
			bucket = cluster.openBucket("reimburseData");
			System.out.println("bucket open........");
			JsonObject products = JsonObject.create();
			String keyCouch = model.getProcessId();
			JsonObject obj = JsonObject.fromJson(message.body().toString());
			JsonDocument doc = JsonDocument.create(keyCouch, obj);
			bucket.upsert(doc);
			// N1qlQuery qlQuery = N1qlQuery.simple("UPSERT INTO `reimburseData` (KEY,
			// VALUE) VALUES
			// ('"+keyCouch+"',"+Json.decodeValue(message.body().toString())+")");
			/*
			 * System.out.println(qlQuery); N1qlQueryResult n1qlQueryResult =
			 * bucket.query(qlQuery); System.out.println(n1qlQueryResult);
			 */
			message.reply("BERHASIL UPDATE ALL!!! \n ");
		});

		vertx.eventBus().consumer(VertxConstant.ADDRESS_INQUIRY_TO_COUCHBASE, message -> {
			System.out.println("ini consumer inquiry couchbase =============== " + message.body().toString());
			Bucket bucket;

			final StandardRequest request = Json.decodeValue(message.body().toString(), StandardRequest.class);
			String sqlSemua;
			// createSomeData();
			CouchbaseEnvironment env = DefaultCouchbaseEnvironment.builder()
					// this set the IO socket timeout globally, to 45s
					.socketConnectTimeout((int) TimeUnit.SECONDS.toMillis(10000))
					// this sets the connection timeout for openBucket calls globally (unless a
					// particular call provides its own timeout)
					.connectTimeout(TimeUnit.SECONDS.toMillis(12000)).build();
			Cluster cluster = CouchbaseCluster.create(env, "http://127.0.0.1:8091");
			cluster.authenticate("Administrator", "123456");
			bucket = cluster.openBucket("reimburseData");
			JsonObject products = JsonObject.create();

			if (request.getFilter().getStatus() != null && !request.getFilter().getStatus().trim().equals("")) {
				sqlSemua = "SELECT * FROM reimburseData where status = '" + request.getFilter().getStatus() + "'";
			} else {
				sqlSemua = "SELECT * FROM reimburseData";
			}

			N1qlQuery qlQuery = N1qlQuery.simple(sqlSemua);
			N1qlQueryResult n1qlQueryResult = bucket.query(qlQuery);
			JsonArray list = new JsonArray(n1qlQueryResult.allRows().toString());

			io.vertx.core.json.JsonObject obj = new io.vertx.core.json.JsonObject();
			obj.put("data", list);
			message.reply(Json.encode(obj));
			System.out.println(Json.encode(obj));
			
		});
	}
}