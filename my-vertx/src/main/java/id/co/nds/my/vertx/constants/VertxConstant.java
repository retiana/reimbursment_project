package id.co.nds.my.vertx.constants;

public class VertxConstant {
	public static final String ADDRESS_LOGIN = "action.login";
	public static final String ADDRESS_GET_TASK = "action.get.task";
	public static final String ADDRESS_SUBMIT_DATA = "action.submit.data";
	public static final String ADDRESS_SUBMIT_APPROVAL_PM = "action.submit.approval.pm";
	public static final String ADDRESS_SUBMIT_APPROVAL_HR = "action.submit.approval.hr";
	public static final String ADDRESS_SUBMIT_REJECTED = "action.submit.rejected";
	public static final String ADDRESS_CONNECT_TO_BPM ="action.connect.to.bpm";
	public static final String ADDRESS_GET_DETAIL_DATA ="action.get.detail";
	public static final String ADDRESS_INQUIRY_TO_COUCHBASE ="action.inquiry.to.couchbase";
	public static final String ADDRESS_REPORT_DATA = "action.report.data";
	public static final String ADDRESS_APPROVED ="action.approved";
	public static final String ADDRESS_REJECTED ="action.rejected";
	public static final String ADDRESS_PENDING ="action.pending";
	public static final String ADDRESS_INQUIRY_TO_POSTGRES ="action.inquiry.to.postgres";	
	public static final String ADDRESS_INSERT_TX_TO_COUCHBASE ="connect.to.couchbase.insert.tx";
	public static final String ADDRESS_UPDATE_TX_TO_COUCHBASE="connect.to.couchbase.update.tx";
	public static final String ADDRESS_INSERT_LOG_TO_POSTGRES= "connect.to.postgres.log.activity";
	public static final String ADDRESS_INSERT_TX_TO_POSTGRES ="connect.to.postgres.insert.tx";
	public static final String ADDRESS_UPDATE_TX_TO_POSTGRES="connect.to.postgres.update.tx";
	
	public static final String URL_ROUTER_INQUIRY = "/simulation/inquiry";
	public static final String URL_ROUTER_REPORT = "/simulation/report";
	public static final String URL_ROUTER = "/simulation*";
	public static final String URL_ROUTER_LOGIN = "/simulation/login";
	public static final String URL_ROUTER_SUBMIT = "/simulation/submit";
	public static final String URL_ROUTER_APPROVED_PM = "/simulation/approvedPm";

	public static final String URL_ROUTER_APPROVED_HR = "/simulation/approvedHr";
	public static final String URL_ROUTE_REJECTED = "/simulation/rejected";
	public static final String URL_ROUTE_TASK = "/simulation/task";
	public static final String URL_ROUTE_GET_DETAIL = "/simulation/getDetail";
}
