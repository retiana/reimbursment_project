package id.co.nds.my.vertx.bpm;

import java.util.Base64;

import id.co.nds.my.vertx.constants.BPMConstant;
import id.co.nds.my.vertx.constants.VertxConstant;
import id.co.nds.my.vertx.model.BPMStandardResponse;
import id.co.nds.my.vertx.model.ProcessModel;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.MultiMap;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.HttpRequest;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.WebClientOptions;

public class BpmApiVerticle extends AbstractVerticle {

	@Override
	public void start() {
		vertx.eventBus().consumer(VertxConstant.ADDRESS_CONNECT_TO_BPM, message -> {
			WebClientOptions clientOption = new WebClientOptions();
			clientOption.setTrustAll(true);

			WebClient client = WebClient.create(vertx, clientOption);
			ProcessModel model = new ProcessModel();
			model = Json.decodeValue(message.body().toString(), ProcessModel.class);

			String url = "";
			String credential = model.getUserModel().getUserId() + ":" + model.getUserModel().getPassword();
			byte[] encodedBytes = Base64.getEncoder().encode(credential.getBytes());

			HttpRequest<Buffer> request = null;
			if (model.getActionName().equalsIgnoreCase(BPMConstant.ACTION_NAME_EXPOSED_PROCESS)) {
				url = BPMConstant.URL_EXPOSED_PROCESS;
				request = client.getAbs(url);
			} else if (model.getActionName().equalsIgnoreCase(BPMConstant.ACTION_NAME_START_PROCESS)) {
				url = BPMConstant.URL_START_PROCESS;
				request = client.postAbs(url);
				request.addQueryParam("action", "start");
				request.addQueryParam("bpdId", "25.bb1b242f-68c3-4c32-b651-f6720c419f4b");
				request.addQueryParam("branchId", "2063.099a8a47-686a-470a-9e62-b1ad27619d32");
			} else if (model.getActionName().equalsIgnoreCase(BPMConstant.ACTION_NAME_QUERY_AVAILABLE_TASK)) {
				url = BPMConstant.URL_QUERY_AVAILABLE_TASK;
				request = client.getAbs(url);
				request.addQueryParam("interactionFilter", "ASSESS_AND_WORK_ON");
				request.addQueryParam("filterByCurrentUser", "true");
			} else if (model.getActionName().equalsIgnoreCase(BPMConstant.ACTION_NAME_ASSIGN_TO_ME)) {
				url = BPMConstant.URL_ASSIGN_TO_ME + model.getTaskId();
				request = client.putAbs(url);
				request.addQueryParam("action", "assign");
				request.addQueryParam("toMe", "true");
			} else if (model.getActionName().equalsIgnoreCase(BPMConstant.ACTION_NAME_TASK_DETAIL)) {
				url = BPMConstant.URL_TASK_DETAIL + model.getTaskId();
				request = client.getAbs(url);
			} else if (model.getActionName().equalsIgnoreCase(BPMConstant.ACTION_NAME_START_TASK)) {
				url = BPMConstant.URL_START_TASK + model.getTaskId();
				request = client.putAbs(url);
				request.addQueryParam("action", "start");
			} else if (model.getActionName().equalsIgnoreCase(BPMConstant.ACTION_NAME_SET_DATA)) {
				JsonObject inputData = new JsonObject();
				inputData.put("input1", (JsonObject) Json.decodeValue(Json.encode(model.getDataModel())));
				inputData.put("output1", (JsonObject) Json.decodeValue(Json.encode(model.getDataModel())));
				inputData.getJsonObject("input1").remove("submitDate");
				inputData.getJsonObject("output1").remove("submitDate");
				url = BPMConstant.URL_SET_DATA + model.getTaskId();
				request = client.putAbs(url);
				request.addQueryParam("action", "setData");
				request.addQueryParam("params", Json.encode(inputData).replace("\\", ""));
			} else if (model.getActionName().equalsIgnoreCase(BPMConstant.ACTION_NAME_GET_DATA)) {
				url = BPMConstant.URL_GET_DATA + model.getTaskId();
				System.out.println("=========================="+url+"=======================");
				request = client.getAbs(url);
				request.addQueryParam("action", "getData");
				request.addQueryParam("fields", "input1");
			} else if (model.getActionName().equalsIgnoreCase(BPMConstant.ACTION_NAME_COMPLETE_TASK)) {
				url = BPMConstant.URL_COMPLETE_TASK + model.getTaskId();
				request = client.putAbs(url);
				request.addQueryParam("action", "complete");
			} else if (model.getActionName().equalsIgnoreCase(BPMConstant.ACTION_NAME_FINISH_TASK)) {
				url = BPMConstant.URL_FINISH_TASK + model.getTaskId();
				request = client.putAbs(url);
				request.addQueryParam("action", "finish");
			}

			MultiMap headers = request.headers();
			headers.set("Authorization", "Basic " + new String(encodedBytes));
			System.out.println(request.headers().toString());
			System.out.println(request.toString());
			request.send(result -> {
				BPMStandardResponse bpmResp = new BPMStandardResponse();
				if (result.succeeded()) {
					HttpResponse<Buffer> response = result.result();
					System.out.println("Received response with status code ===" + response.statusCode());
					bpmResp.setStatus(String.valueOf(response.statusCode()));
					if (response.statusCode() == 200) {
						bpmResp.setData(response.bodyAsJsonObject().getJsonObject("data"));
						bpmResp.setDesc("SUCCESS");
					} else if (response.statusCode() == 401) {
						bpmResp.setDesc("GAGAL");
					} else {
						bpmResp.setDesc(response.statusMessage());
						
					}
					message.reply(Json.encode(bpmResp));
				} else {
					System.out.println("Something went wrong " + result.cause().getMessage());
					message.reply(result.cause().getMessage());
				}
			});
		});
	}

}
