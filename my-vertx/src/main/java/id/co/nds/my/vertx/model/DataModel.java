package id.co.nds.my.vertx.model;

import java.util.ArrayList;

public class DataModel {
	
	private String id;
	private String name;
	private String submitDate;
	private double total;
	private String status;
	private String hrApprove;
	private String pmApprove;
	private String notes;
	private String department;
	private String position;
	private String reimburstType;
	private ArrayList<TransactionModel> listReimburstment;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSubmitDate() {
		return submitDate;
	}
	public void setSubmitDate(String submitDate) {
		this.submitDate = submitDate;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getHrApprove() {
		return hrApprove;
	}
	public void setHrApprove(String hrApprove) {
		this.hrApprove = hrApprove;
	}
	public String getPmApprove() {
		return pmApprove;
	}
	public void setPmApprove(String pmApprove) {
		this.pmApprove = pmApprove;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getReimburstType() {
		return reimburstType;
	}
	public void setReimburstType(String reimburstType) {
		this.reimburstType = reimburstType;
	}
	public ArrayList<TransactionModel> getListReimburstment() {
		return listReimburstment;
	}
	public void setListReimburstment(ArrayList<TransactionModel> listReimburstment) {
		this.listReimburstment = listReimburstment;
	}
	
}
