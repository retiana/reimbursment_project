package id.co.nds.my.vertx;

import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;

import id.co.nds.my.vertx.constants.BPMConstant;
import id.co.nds.my.vertx.constants.VertxConstant;
import id.co.nds.my.vertx.model.ProcessModel;
import id.co.nds.my.vertx.model.UserModel;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.Json;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CorsHandler;

public class RestApiVerticle extends AbstractVerticle {
	
	private static final Logger logger = Logger.getLogger(RestApiVerticle.class);
	
	@Override
	public void start(Future<Void> startFuture){
		
		Router router = Router.router(vertx);
		Set<String> allowedHeaders = new HashSet<>();
		allowedHeaders.add("x-requested-with");
		allowedHeaders.add("Access-Control-Allow-Origin");
		allowedHeaders.add("origin");
		allowedHeaders.add("Content-Type");
		allowedHeaders.add("accept");
		allowedHeaders.add("X-PINGARUNER");

		Set<HttpMethod> allowedMethods = new HashSet<>();
		allowedMethods.add(HttpMethod.GET);
		allowedMethods.add(HttpMethod.POST);
		allowedMethods.add(HttpMethod.OPTIONS);
		allowedMethods.add(HttpMethod.DELETE);
		allowedMethods.add(HttpMethod.PATCH);
		allowedMethods.add(HttpMethod.PUT);
		router.route().handler(CorsHandler.create("*").allowedHeaders(allowedHeaders).allowedMethods(allowedMethods));

		router.route(VertxConstant.URL_ROUTER).handler(BodyHandler.create());
		router.post(VertxConstant.URL_ROUTER_LOGIN).handler(this::login);
		router.post(VertxConstant.URL_ROUTER_SUBMIT).handler(this::submit);
		router.post(VertxConstant.URL_ROUTER_APPROVED_PM).handler(this::approvedPm);

		router.post(VertxConstant.URL_ROUTER_APPROVED_HR).handler(this::approvedHr);
		router.post(VertxConstant.URL_ROUTE_TASK).handler(this::getTask);
		router.post(VertxConstant.URL_ROUTE_GET_DETAIL).handler(this::getDetail);
		router.post(VertxConstant.URL_ROUTER_INQUIRY).handler(this::inquiry);
		router.post(VertxConstant.URL_ROUTER_REPORT).handler(this::report);
		
		
		vertx
			.createHttpServer()
			.requestHandler(router::accept)
			.listen(
		         // Retrieve the port from the configuration,
		         // default to 8080.
		         config().getInteger("http.port", 9004),
		         result -> {
		           if (result.succeeded()) {
		        	   startFuture.complete();
		           } else {
		        	   startFuture.fail(result.cause());
		           }
		         }
		     );
	}
	private void inquiry(RoutingContext routingContext) {
		vertx.eventBus().send(VertxConstant.ADDRESS_INQUIRY_TO_COUCHBASE, routingContext.getBodyAsString(), response -> {
          	if (response.succeeded()) {
            	routingContext.response().end(response.result().body().toString());
            } else {
                logger.error("Can't send message to hello service", response.cause());
                routingContext.response().setStatusCode(500).end(response.cause().getMessage());
            }
        });
	}
	
	private void report(RoutingContext routingContext) {
		vertx.eventBus().send(VertxConstant.ADDRESS_REPORT_DATA, routingContext.getBodyAsString(), response -> {
		
			if (response.succeeded()) {
            	routingContext.response().end(response.result().body().toString());
            } else {
                logger.error("Can't send message to hello service", response.cause());
                routingContext.response().setStatusCode(500).end(response.cause().getMessage());
            }
        });
	}
	private void getDetail(RoutingContext routingContext) {
		System.out.println("\n\n\n NEMBAK GET DETAIL YAAAAAA");
		vertx.eventBus().send(VertxConstant.ADDRESS_GET_DETAIL_DATA, routingContext.getBodyAsString(), response -> {
			if (response.succeeded()) {
            	routingContext.response().end(response.result().body().toString());
            } else {
                logger.error("Can't send message to hello service", response.cause());
                routingContext.response().setStatusCode(500).end(response.cause().getMessage());
            }
        });
	}
	
	
	private void login(RoutingContext routingContext) {
		System.out.println("\n\n\n NEMBAK LOGIN YAAAAAA");
		vertx.eventBus().send(VertxConstant.ADDRESS_LOGIN, routingContext.getBodyAsString(), response -> {
            if (response.succeeded()) {
            	routingContext.response().end(response.result().body().toString());
            	System.out.println(routingContext.getBodyAsString());
            } else {
                logger.error("Can't send message to hello service", response.cause());
                routingContext.response().setStatusCode(500).end(response.cause().getMessage());
            }
        });
	}
	
	private void getTask(RoutingContext routingContext) {
		System.out.println("\n\n\n NEMBAK GET TASK YAAAAAA");
		vertx.eventBus().send(VertxConstant.ADDRESS_GET_TASK, routingContext.getBodyAsString(), response -> {
            if (response.succeeded()) {
            	routingContext.response().end(response.result().body().toString());
            	System.out.println(routingContext.getBodyAsString());
            } else {
                logger.error("Can't send message to hello service", response.cause());
                routingContext.response().setStatusCode(500).end(response.cause().getMessage());
            }
        });
	}
	
	private void submit(RoutingContext routingContext) {
		System.out.println("\n\n\n NEMBAK SUBMIT YAAAAAA");
		System.out.println(routingContext);
		vertx.eventBus().send(VertxConstant.ADDRESS_SUBMIT_DATA, routingContext.getBodyAsString(), response -> {
            if (response.succeeded()) {
            	routingContext.response().end(response.result().body().toString());
            	System.out.println(routingContext.getBodyAsString());
            } else {
                logger.error("Can't send message to hello service", response.cause());
                routingContext.response().setStatusCode(500).end(response.cause().getMessage());
            }
        });
	}
	
	private void approvedPm(RoutingContext routingContext) {
		System.out.println("\n\n\n NEMBAK APPROVED PM YAAAAAA");
		vertx.eventBus().send(VertxConstant.ADDRESS_SUBMIT_APPROVAL_PM, routingContext.getBodyAsString(), response -> {
            if (response.succeeded()) {
            	routingContext.response().end(response.result().body().toString());
            } else {
                logger.error("Can't send message to hello service", response.cause());
                routingContext.response().setStatusCode(500).end(response.cause().getMessage());
            }
        });
	}
	
	private void approvedHr(RoutingContext routingContext) {
		System.out.println("\n\n\n NEMBAK APPROVED HR YAAAAAA");
		vertx.eventBus().send(VertxConstant.ADDRESS_SUBMIT_APPROVAL_HR, routingContext.getBodyAsString(), response -> {
            if (response.succeeded()) {
            	routingContext.response().end(response.result().body().toString());
            } else {
                logger.error("Can't send message to hello service", response.cause());
                routingContext.response().setStatusCode(500).end(response.cause().getMessage());
            }
        });
	}
	
	private void addOne(RoutingContext routingContext) {
		System.out.println("\n\n\n\n REQUEST>>>"+routingContext.getBodyAsString());
		//final ModelRequest request = Json.decodeValue(routingContext.getBodyAsString(), ModelRequest.class);
		//request.setParam1("SUCCESS");
		
		/* Invoke using the event bus. */
		UserModel user = new UserModel();
		user.setUserId("usercoba1");
		user.setPassword("admin");
		ProcessModel model = new ProcessModel();
		model.setUserModel(user);
		model.setActionName(BPMConstant.ACTION_NAME_START_TASK);
		model.setTaskId("653");
        vertx.eventBus().send("connect.to.bpm", Json.encode(model), response -> {

            if (response.succeeded()) {
                /* Send the result from HelloWorldService to the http connection. */
            	routingContext.response().end(response.result().body().toString());
            } else {
                logger.error("Can't send message to hello service", response.cause());
                routingContext.response().setStatusCode(500).end(response.cause().getMessage());
            }
        });
        
		/*routingContext.response()
	      	.setStatusCode(200)
	      	.putHeader("content-type", "application/json; charset=utf-8")
	      	.end(Json.encodePrettily(request));*/
	}

}
