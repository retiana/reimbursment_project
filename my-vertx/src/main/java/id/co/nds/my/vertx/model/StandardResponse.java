package id.co.nds.my.vertx.model;

import java.util.ArrayList;

public class StandardResponse {

	private String statusCode;
	private String statusDesc;
	private UserModel user;
	private ArrayList<ProcessModel> data;

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusDesc() {
		return statusDesc;
	}

	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}

	public UserModel getUser() {
		return user;
	}

	public void setUser(UserModel user) {
		this.user = user;
	}

	public ArrayList<ProcessModel> getData() {
		return data;
	}

	public void setData(ArrayList<ProcessModel> data) {
		this.data = data;
	}

}
