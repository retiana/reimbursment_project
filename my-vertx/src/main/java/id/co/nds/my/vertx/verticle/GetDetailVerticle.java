package id.co.nds.my.vertx.verticle;

import java.util.ArrayList;

import id.co.nds.my.vertx.constants.BPMConstant;
import id.co.nds.my.vertx.constants.VertxConstant;
import id.co.nds.my.vertx.model.BPMStandardResponse;
import id.co.nds.my.vertx.model.DataModel;
import id.co.nds.my.vertx.model.ModelLogActivity;
import id.co.nds.my.vertx.model.ProcessModel;
import id.co.nds.my.vertx.model.StandardRequest;
import id.co.nds.my.vertx.model.StandardResponse;
import id.co.nds.my.vertx.model.TransactionModel;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public class GetDetailVerticle extends AbstractVerticle {
	@Override
	public void start() {
		vertx.eventBus().consumer(VertxConstant.ADDRESS_GET_DETAIL_DATA, message -> {
			System.out.println("ini eventbus consumer getdetail = "+message.body().toString());
			final StandardRequest request = Json.decodeValue(message.body().toString(), StandardRequest.class);
			final ProcessModel model = request.getData();
			final StandardResponse resp = new StandardResponse();
			final DataModel data = new DataModel();
			final ArrayList<TransactionModel> listTransaction = new ArrayList<>();
			final ArrayList<ProcessModel> listProcess = new ArrayList<>();
			model.setUserModel(request.getUser());
		
			model.setActionName(BPMConstant.ACTION_NAME_GET_DATA);
			vertx.eventBus().send(VertxConstant.ADDRESS_CONNECT_TO_BPM, Json.encode(model), response -> {
				if (response.succeeded()) {
					BPMStandardResponse bpmResp1 = Json.decodeValue(response.result().body().toString(), BPMStandardResponse.class);
					resp.setStatusCode(bpmResp1.getStatus());
					resp.setStatusDesc(bpmResp1.getDesc());
					resp.setUser(model.getUserModel());
					System.out.println("\n\n RESPONSE BPM>>>"+response.result().body().toString());
					if (bpmResp1.getStatus().equalsIgnoreCase("200")) {
						JsonObject objDataInput = (JsonObject) Json.decodeValue(Json.encode(bpmResp1.getData()));
						JsonObject objResultMap = objDataInput.getJsonObject("resultMap");
						JsonObject objResult = objResultMap.getJsonObject("input1");
						data.setId(objResult.getString("id"));
						data.setName(objResult.getString("name"));
						data.setPosition(objResult.getString("position"));
						data.setDepartment(objResult.getString("department"));
						data.setReimburstType(objResult.getString("reimburstType"));
						data.setTotal(objResult.getDouble("total"));
						data.setHrApprove(objResult.getString("hrApprove"));
						data.setPmApprove(objResult.getString("pmApprove"));
						data.setStatus(objResult.getString("status"));
					
						JsonObject listData = objResult.getJsonObject("listReimburstment");
						JsonArray listReim = listData.getJsonArray("items");
						if (listReim != null && listReim.size() > 0) {
							for (int j = 0; j < listReim.size(); j++) {
								JsonObject objTransaction = listReim.getJsonObject(j);
								TransactionModel transaction = new TransactionModel();
								transaction.setDate(objTransaction.getString("date"));
								transaction.setDescription(objTransaction.getString("description"));
								transaction.setSubTotal(objTransaction.getDouble("subTotal"));
								listTransaction.add(transaction);
							}
						}
						data.setListReimburstment(listTransaction);
						model.setDataModel(data);
						
						listProcess.add(model);
						resp.setData(listProcess);
						message.reply(Json.encode(resp));
						
						ModelLogActivity logGetDetail = new ModelLogActivity();
						logGetDetail.setId(String.valueOf(System.currentTimeMillis()));
						logGetDetail.setRequestdata(message.body().toString());
						logGetDetail.setResponsedata(Json.encode(resp));
						logGetDetail.setStatuscode(resp.getStatusCode());
						logGetDetail.setTaskname("Get Detail");
						
						vertx.eventBus().send(VertxConstant.ADDRESS_INSERT_LOG_TO_POSTGRES, Json.encode(logGetDetail),
								responseLog -> {
									if (responseLog.succeeded()) {
										
									} else {
										message.reply(responseLog.cause().getMessage());
									}
								});
					}
				}else {
					message.reply(response.cause().getMessage());
				}
			});
		});
	}
}
