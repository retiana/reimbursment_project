package id.co.nds.my.vertx.model;

import java.sql.Timestamp;
import java.util.ArrayList;

public class ModelLogActivity {
	
	private String id;
	private String statuscode;
	private String responsedata;
	private String requestdata;
	private String taskname;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getStatuscode() {
		return statuscode;
	}
	public void setStatuscode(String statuscode) {
		this.statuscode = statuscode;
	}
	public String getResponsedata() {
		return responsedata;
	}
	public void setResponsedata(String responsedata) {
		this.responsedata = responsedata;
	}
	public String getRequestdata() {
		return requestdata;
	}
	public void setRequestdata(String requestdata) {
		this.requestdata = requestdata;
	}
	public String getTaskname() {
		return taskname;
	}
	public void setTaskname(String taskname) {
		this.taskname = taskname;
	}
	
}
