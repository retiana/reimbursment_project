package id.co.nds.my.vertx.verticle;

import java.sql.Timestamp;
import java.util.ArrayList;

import javax.imageio.plugins.bmp.BMPImageWriteParam;

import org.apache.log4j.Logger;

import id.co.nds.my.vertx.constants.BPMConstant;
import id.co.nds.my.vertx.constants.VertxConstant;
import id.co.nds.my.vertx.model.BPMStandardResponse;
import id.co.nds.my.vertx.model.ModelLogActivity;
import id.co.nds.my.vertx.model.ProcessModel;
import id.co.nds.my.vertx.model.StandardRequest;
import id.co.nds.my.vertx.model.StandardResponse;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public class LoginVerticle extends AbstractVerticle {

	private static final Logger logger = Logger.getLogger(LoginVerticle.class);

	@Override
	public void start() {
		vertx.eventBus().consumer(VertxConstant.ADDRESS_LOGIN, message -> {
			final ProcessModel model = new ProcessModel();
			// System.out.println("ini message from eventbus consumer address login
			// ="+message.body().toString());
			final StandardRequest request = Json.decodeValue(message.body().toString(), StandardRequest.class);
			model.setUserModel(request.getUser());
			model.setActionName(BPMConstant.ACTION_NAME_QUERY_AVAILABLE_TASK);

			vertx.eventBus().send(VertxConstant.ADDRESS_CONNECT_TO_BPM, Json.encode(model), response -> {
				
				StandardResponse resp = new StandardResponse();
				if (response.succeeded()) {
					// System.out.println("ini message from eventbus sender address bpm
					// ="+Json.decodeValue(response.result().body().toString()));
					BPMStandardResponse bpmResp = Json.decodeValue(response.result().body().toString(),
							BPMStandardResponse.class);
					resp.setStatusCode(bpmResp.getStatus());
					resp.setStatusDesc(bpmResp.getDesc());
					resp.setUser(request.getUser());
					ArrayList<ProcessModel> listAvailable = new ArrayList<>();

					if (bpmResp.getStatus().equalsIgnoreCase("200")) {
						JsonObject objData = (JsonObject) Json.decodeValue(Json.encode(bpmResp.getData()));
						JsonArray list = objData.getJsonArray("items");
						if (list != null && list.size() > 0) {
							for (int i = 0; i < list.size(); i++) {
								JsonObject objAvailable = list.getJsonObject(i);
								ProcessModel availableTask = new ProcessModel();
								availableTask.setBpId(objAvailable.getString("PT_PTID"));
								availableTask.setProcessId(objAvailable.getString("PROCESS_INSTANCE.PIID"));
								availableTask.setProcessName(objAvailable.getString("PROCESS_APP_ACRONYM"));
								availableTask.setStatus("PENDING");
								availableTask.setTaskId(objAvailable.getString("TASK.TKIID"));
								availableTask.setTaskName(objAvailable.getString("NAME"));
								listAvailable.add(availableTask);
							}
						}else if (bpmResp.getStatus().equalsIgnoreCase("401")) {
							resp.setStatusCode(bpmResp.getStatus());
							resp.setStatusDesc(bpmResp.getDesc());
							resp.setUser(request.getUser());
						}
					} 
					resp.setData(listAvailable);
					message.reply(Json.encode(resp));

					ModelLogActivity logActvity = new ModelLogActivity();
					String a = Json.encode(resp.getUser());
					logActvity.setRequestdata(message.body().toString());
					logActvity.setResponsedata(Json.encode(resp));
					logActvity.setStatuscode(resp.getStatusCode());
					logActvity.setTaskname("Login");
					logActvity.setId(String.valueOf(System.currentTimeMillis()));
					vertx.eventBus().send(VertxConstant.ADDRESS_INSERT_LOG_TO_POSTGRES, Json.encode(logActvity),
							responseLog -> {
								if (responseLog.succeeded()) {
								} else {
									message.reply(responseLog.cause().getMessage());
								}
							});
				} else {

					message.reply(response.cause().getMessage());
				}
			});
		});
	}
}
