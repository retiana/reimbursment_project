package id.co.nds.my.vertx;
import org.apache.log4j.Logger;
import id.co.nds.my.vertx.bpm.BpmApiVerticle;
import id.co.nds.my.vertx.verticle.GetDetailVerticle;
import id.co.nds.my.vertx.verticle.GetTaskVerticle;
import id.co.nds.my.vertx.verticle.HrApprovalVerticle;
import id.co.nds.my.vertx.verticle.LoginVerticle;
import id.co.nds.my.vertx.verticle.PmApprovalVerticle;
import id.co.nds.my.vertx.verticle.SubmitDataVerticle;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Vertx;

public class MainVerticle extends AbstractVerticle {

	private static final Logger logger = Logger.getLogger(MainVerticle.class);

	public static void main(String[] args) throws InterruptedException {
		Vertx vertx = Vertx.vertx();
		vertx.deployVerticle(new MainVerticle());
	}

	@Override
	public void start(Future<Void> startFuture) throws Exception {
		logger.info("\n\n\n\n Deployed main module " + startFuture + Thread.currentThread().getName());
		vertx.deployVerticle(new RestApiVerticle());
		vertx.deployVerticle(new PostgresVerticle());
		vertx.deployVerticle(new CouchbaseVerticle());
		vertx.deployVerticle(new BpmApiVerticle());
		vertx.deployVerticle(new LoginVerticle());
		vertx.deployVerticle(new SubmitDataVerticle());
		vertx.deployVerticle(new PmApprovalVerticle());
		vertx.deployVerticle(new HrApprovalVerticle());
		vertx.deployVerticle(new GetDetailVerticle());
		vertx.deployVerticle(new GetTaskVerticle());
	}

	@Override
	public void stop() throws Exception {
		logger.info("\n\n\n\n Successfully stopping verticle.");
	}
}
