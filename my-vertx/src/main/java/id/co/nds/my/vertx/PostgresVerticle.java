package id.co.nds.my.vertx;

import org.apache.log4j.Logger;

import id.co.nds.my.vertx.constants.VertxConstant;
import id.co.nds.my.vertx.model.DataModel;
import id.co.nds.my.vertx.model.ModelLogActivity;
import id.co.nds.my.vertx.model.ProcessModel;
import id.co.nds.my.vertx.model.StandardRequest;
import id.co.nds.my.vertx.model.StandardResponse;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.asyncsql.PostgreSQLClient;
import io.vertx.ext.jdbc.JDBCClient;
import io.vertx.ext.sql.SQLClient;
import io.vertx.ext.sql.SQLConnection;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import io.vertx.ext.sql.ResultSet;

public class PostgresVerticle extends AbstractVerticle {

	private static final Logger logger = Logger.getLogger(PostgresVerticle.class);

	@Override
	public void start() {
		vertx.eventBus().consumer(VertxConstant.ADDRESS_INSERT_LOG_TO_POSTGRES, message -> {
			final ModelLogActivity logActvity = Json.decodeValue(message.body().toString(), ModelLogActivity.class);
			JDBCClient jdbc = JDBCClient.createNonShared(vertx,
					new JsonObject().put("url", "jdbc:postgresql://localhost:5432/customer").put("user", "postgres")
							.put("password", "reti").put("driver_class", "org.postgresql.Driver"));
			SQLClient psqlClient = PostgreSQLClient.createNonShared(vertx,
					new JsonObject().put("host", "localhost").put("username", "postgres").put("password", "reti")
							.put("database", "customer").put("maxPoolSize", 10).put("connectTimeout", 120000)
							.put("queryTimeout", 60000).put("maxConnectionRetries", 2));
			psqlClient.getConnection(res -> {
				if (res.succeeded()) {
					SQLConnection connection = res.result();
					String sql = "INSERT INTO log_activity_service (id,statuscode,responsedata,requestdata,taskname,createdate) "
							+ "VALUES ('" + logActvity.getId() + "','" + logActvity.getStatuscode() + "','"
							+ logActvity.getResponsedata() + "','" + logActvity.getRequestdata() + "','"
							+ logActvity.getTaskname() + "',now())";
					connection.setAutoCommit(false, ar -> connection.execute(sql, result -> {
						if (result.succeeded()) {

							System.out.println("log activity di catat");
							connection.commit(null);
						} else {
							System.out.println("ROLLBACK");
							connection.rollback(null);
						}
						connection.close();
						message.reply("SUCCESS");
					}));

				} else {
					message.reply(res.cause());
				}
			});

		});

		vertx.eventBus().consumer(VertxConstant.ADDRESS_INSERT_TX_TO_POSTGRES, message -> {
			System.out.println(
					"ini di postgresql consumer event bus insert tx =================== " + message.body().toString());
			final ProcessModel data = Json.decodeValue(message.body().toString(), ProcessModel.class);
			JDBCClient jdbc = JDBCClient.createNonShared(vertx,
					new JsonObject().put("url", "jdbc:postgresql://localhost:5432/customer").put("user", "postgres")
							.put("password", "reti").put("driver_class", "org.postgresql.Driver"));

			SQLClient psqlClient = PostgreSQLClient.createNonShared(vertx,
					new JsonObject().put("host", "localhost").put("username", "postgres").put("password", "reti")
							.put("database", "customer").put("maxPoolSize", 10).put("connectTimeout", 120000)
							.put("queryTimeout", 60000).put("maxConnectionRetries", 2));

			psqlClient.getConnection(res -> {
				if (res.succeeded()) {
					SQLConnection connection = res.result();
					String a = data.getProcessId();
					String b = data.getProcessName();
					String c = data.getBpId();
					String d = data.getTaskId();
					String e = data.getTaskName();
					String f = data.getStatus();
					String g = data.getBranchId();
					String h = Json.encode(data.getUserModel());
					String i = Json.encode(data.getDataModel());
					String j = data.getActionName();
					String sql = "INSERT INTO ms_transaction (processId ,processName ,bpId ,taskId ,taskName ,status ,branchId ,userModel ,dataModel ,actionName,date,notes )"
							+ " VALUES ('" + a + "','" + b + "','" + c + "','" + d + "','" + e + "','" + f + "','" + g
							+ "','" + h + "','" + i + "','" + j + "',now(),'belum ada')";
					connection.setAutoCommit(false, ar -> connection.execute(sql, result -> {
						if (result.succeeded()) {

							System.out.println("insert data reimbursement di postgresql");
							connection.commit(null);
						} else {
							System.out.println("ROLLBACK");
							connection.rollback(null);
						}
						connection.close();
						message.reply("SUCCESS");
					}));

				} else {
					message.reply(res.cause());
				}
			});

		});

		vertx.eventBus().consumer(VertxConstant.ADDRESS_UPDATE_TX_TO_POSTGRES, message -> {

			System.out.println("ini di postgresql consumer event bus update tx approval pm dan hr ============ "
					+ message.body().toString());
			final ProcessModel data = Json.decodeValue(message.body().toString(), ProcessModel.class);

			JDBCClient jdbc = JDBCClient.createNonShared(vertx,
					new JsonObject().put("url", "jdbc:postgresql://localhost:5432/customer").put("user", "postgres")
							.put("password", "reti").put("driver_class", "org.postgresql.Driver"));

			SQLClient psqlClient = PostgreSQLClient.createNonShared(vertx,
					new JsonObject().put("host", "localhost").put("username", "postgres").put("password", "reti")
							.put("database", "customer").put("maxPoolSize", 10).put("connectTimeout", 120000)
							.put("queryTimeout", 60000).put("maxConnectionRetries", 2));
			psqlClient.getConnection(res -> {
				if (res.succeeded()) {
					SQLConnection connection = res.result();
					String a = data.getProcessId();
					// String b = data.getProcessName();
					// String c = data.getBpId();
					String d = data.getTaskId();
					String e = data.getTaskName();
					String f = data.getStatus();
					// String g = data.getBranchId();
					String h = Json.encode(data.getUserModel());
					String i = Json.encode(data.getDataModel());
					String j = data.getActionName();

					String sql = "update ms_transaction set taskId = '" + d + "', taskName = '" + e + "',status ='" + f
							+ "', userModel ='" + h + "', dataModel='" + i + "', actionName='" + j + "', notes = '"
							+ data.getReason() + "' where processId ='" + a + "' ";
					connection.setAutoCommit(false, ar -> connection.execute(sql, result -> {
						if (result.succeeded()) {

							System.out.println("update data reimbursement di postgresql");
							connection.commit(null);
						} else {
							System.out.println("ROLLBACK");
							connection.rollback(null);
						}
						connection.close();
						message.reply("SUCCESS");
					}));
				} else {
					message.reply(res.cause());
				}
			});
		});

		vertx.eventBus().consumer(VertxConstant.ADDRESS_REPORT_DATA, message -> {
			System.out.println("ini consumer report data postgresql=========" + message.body().toString());
			final StandardRequest request = Json.decodeValue(message.body().toString(), StandardRequest.class);
			JDBCClient jdbc = JDBCClient.createNonShared(vertx,
					new JsonObject().put("url", "jdbc:postgresql://localhost:5432/customer").put("user", "postgres")
							.put("password", "reti").put("driver_class", "org.postgresql.Driver"));

			SQLClient psqlClient = PostgreSQLClient.createNonShared(vertx,
					new JsonObject().put("host", "localhost").put("username", "postgres").put("password", "reti")
							.put("database", "customer").put("maxPoolSize", 10).put("connectTimeout", 120000)
							.put("queryTimeout", 60000).put("maxConnectionRetries", 2));

			psqlClient.getConnection(res -> {

				if (res.succeeded()) {
					SQLConnection connection = res.result();

					String sql;
					if (request.getFilter().getStart() != null && request.getFilter().getEnd() != null
							&& request.getFilter().getStatus() != null) {
						String dateFrom = "";
						String dateEnd = "";
						try {
							dateFrom = new SimpleDateFormat("yyyy-MM-dd 00:00:00")
									.format(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS")
											.parse(request.getFilter().getStart()));
							dateEnd = new SimpleDateFormat("yyyy-MM-dd 23:59:59")
									.format(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS")
											.parse(request.getFilter().getEnd()));
						} catch (ParseException ex) {
							// TODO Auto-generated catch block
							ex.printStackTrace();
							message.reply(ex.getMessage());
						}
						sql = "SELECT datamodel FROM ms_transaction where date between '" + dateFrom + "' and '"
								+ dateEnd + "'" + "and lower(status) = '"
								+ request.getFilter().getStatus().toLowerCase() + "'";
					} else {
						sql = "SELECT datamodel FROM ms_transaction";
					}
					connection.setAutoCommit(false, ar -> connection.query(sql, result -> {
						if (result.succeeded()) {
							connection.commit(null);
							ResultSet resultset = result.result();
							// System.out.println("\n\n\n MASUKKKKKKKKKK");
							JsonObject object = new JsonObject();
							List<JsonObject> listData = result.result().getRows();
							System.out.println("\n\n\n MASUKKKKKKKKKK222>>" + listData.size());
							ArrayList<DataModel> listModel = new ArrayList<>();
							if (listData != null && listData.size() > 0) {
								for (int i = 0; i < listData.size(); i++) {
									// System.out.println("\n\n\n MASUKKKKKKKKKK1");
									DataModel model = new DataModel();
									System.out.println(
											"\n\n\n MASUKKKKKKKKKK2>>" + listData.get(i).getString("datamodel"));
									model = Json.decodeValue(listData.get(i).getString("datamodel"), DataModel.class);
									listModel.add(model);
									// System.out.println("\n\n\n MASUKKKKKKKKKK3");
								}
							}
							object.put("data", listModel);
							message.reply(Json.encode(object));
							System.out.println("ini data report ===========" + Json.encode(object));
						} else {
							connection.rollback(null);
							message.reply(result.cause().getMessage());
						}
						connection.close();
						// System.out.println("\n\n\n\n HALOOOOOOOO");
					}));

				} else {

					message.reply(res.cause());
				}
			});

		});
	}

}
