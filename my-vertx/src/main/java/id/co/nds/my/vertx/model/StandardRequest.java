package id.co.nds.my.vertx.model;

public class StandardRequest {
	
	private UserModel user;
	private ProcessModel data;
	private Filter filter;
	private String reason;
	
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public Filter getFilter() {
		return filter;
	}
	public void setFilter(Filter filter) {
		this.filter = filter;
	}
	public UserModel getUser() {
		return user;
	}
	public void setUser(UserModel user) {
		this.user = user;
	}
	public ProcessModel getData() {
		return data;
	}
	public void setData(ProcessModel data) {
		this.data = data;
	}
	
}
