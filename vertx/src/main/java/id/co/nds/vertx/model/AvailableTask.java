package id.co.nds.vertx.model;

public class AvailableTask {
private String taskName;
private String status;
private String taskId;
private String processId;

public String getTaskName() {
	return taskName;
}
public void setTaskName(String taskName) {
	this.taskName = taskName;
}
public String getStatus() {
	return status;
}
public void setStatus(String status) {
	this.status = status;
}
public String getTaskId() {
	return taskId;
}
public void setTaskId(String taskId) {
	this.taskId = taskId;
}
public String getProcessId() {
	return processId;
}
public void setProcessId(String processId) {
	this.processId = processId;
}


}
