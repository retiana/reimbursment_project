package id.co.nds.vertx.model;

import java.util.ArrayList;

public class ReimburseData {
	private ArrayList<LoginModel> loginData;
	private ArrayList<InputRequest> InputData;

	public ArrayList<LoginModel> getLoginData() {
		return loginData;
	}

	public void setLoginData(ArrayList<LoginModel> loginData) {
		this.loginData = loginData;
	}

	public ArrayList<InputRequest> getInputData() {
		return InputData;
	}

	public void setInputData(ArrayList<InputRequest> inputData) {
		InputData = inputData;
	}

}
