package id.co.nds.vertx.model;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.internal.LinkedTreeMap;

import id.co.nds.core.exception.AppException;
import id.co.nds.core.exception.GeneralException;
import id.co.nds.util.Util;

public class OrderFlowFcd {

	private static Gson gson = new Gson();
	private static JsonParser parser = new JsonParser();
	private static final Integer TIMEOUT = 30 * 1000;
	protected static final String URL_BPM = "https://192.168.163.135:9443/rest/bpm/wle/v1";
	
	public static String startBPM(String bpdId,String processId) {
		String url = URL_BPM+"/process?";
		RequestBPMParam bpmParam = new RequestBPMParam();
		bpmParam.setAction("start");
		bpmParam.setBpdId(bpdId);
		bpmParam.setProcessAppId(processId);
		bpmParam.setParts("all");
		String param;
		try {
			param = ObjectToParamString(bpmParam, RequestBPMParam.class);
			String urlParam = url+param.substring(0, param.length()-1);
			String response = callService(urlParam, "POST");
			JsonParser parser = new JsonParser();
			
			JsonObject responseObj = (JsonObject) parser.parse(response);
			String pid = responseObj.get("data").getAsJsonObject().get("piid").getAsString();
			return pid;
		} catch (GeneralException e) {
			e.printStackTrace();
		}catch (SocketTimeoutException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (AppException e) {
			e.printStackTrace();
		}
		
		return "";
	}
	
	public static String finishTask(String taskId,JSONObject params) {
		String url = URL_BPM+"/task/"+taskId+"+?";
		RequestBPMParam bpmParam = new RequestBPMParam();
		bpmParam.setAction("finish");
		bpmParam.setParts("all");
		if (params != null) {
			bpmParam.setParams(params);
		}
		String param;
		try {
			param = ObjectToParamString(bpmParam, RequestBPMParam.class);
			String urlParam = url+param.substring(0, param.length()-1);
			String response = callService(urlParam, "POST");
			JsonParser parser = new JsonParser();
			
			JsonObject responseObj = (JsonObject) parser.parse(response);
			String state = responseObj.get("data").getAsJsonObject().get("state").getAsString();
			String pid = responseObj.get("data").getAsJsonObject().get("state").getAsString();
			return pid;
		} catch (GeneralException e) {
			e.printStackTrace();
		}catch (SocketTimeoutException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (AppException e) {
			e.printStackTrace();
		}
		
		return "";
	}
	//get model
	 //https://ibmbpm-rpa:9443/rest/bpm/wle/v1/processModel/25.bd3ebd82-cfe4-4e6c-a93a-8cd4d5ef8c27?snapshotId=2064.c9c4e8ed-fad0-4706-8b33-a9cc79608853&parts=all
	public static JSONObject getModel(String bpdId,String snapshotId) {
		String url = URL_BPM+"/processModel/"+bpdId+"?snapshotId="+snapshotId+"&parts=dataModel";
		try {
			String response = callService(url, "GET");
			JsonParser parser = new JsonParser();
			JsonObject responseObj = (JsonObject) parser.parse(response);
			JsonObject dataModel = responseObj.get("data").getAsJsonObject().get("DataModel").getAsJsonObject();
			parseModel(dataModel);
			
		} catch (SocketTimeoutException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (AppException e) {
			e.printStackTrace();
		}
		
		return new JSONObject();
	}
	
	//claim task URL_BPM+/task/taskId?action="assign"&&toMe="true");
	public static String claimTask(String taskId) {
		String url = URL_BPM+"/task/"+taskId+"+?";
		RequestBPMParam bpmParam = new RequestBPMParam();
		bpmParam.setAction("assign");
		bpmParam.setToMe(true);
		String param;
		try {
			param = ObjectToParamString(bpmParam, RequestBPMParam.class);
			String urlParam = url+param.substring(0, param.length()-1);
			String response = callService(urlParam, "POST");
			
			JsonObject responseObj = (JsonObject) parser.parse(response);
			String state = responseObj.get("data").getAsJsonObject().get("state").getAsString();
			String pid = responseObj.get("data").getAsJsonObject().get("state").getAsString();
			return pid;
		} catch (GeneralException e) {
			e.printStackTrace();
		}catch (SocketTimeoutException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (AppException e) {
			e.printStackTrace();
		}
		
		return "";
	}
	
	private JSONObject getLatestTaskInStack (JSONObject instanceDetail){
		
		TreeMap<Integer, JSONObject> cache = new TreeMap<Integer, JSONObject>();
		JSONObject processDetail = instanceDetail;
		JSONObject data;
		try {
			data = processDetail.getJSONObject("data");
			JSONArray tasks = data.getJSONArray("tasks");
			Integer numOfTasks = tasks.length();
			for (int i=0; i< numOfTasks; i++) {
				JSONObject task = tasks.getJSONObject(i);
				Integer tkiid = task.getInt("tkiid");
				cache.put(tkiid, task);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		Integer lastKey = cache.lastKey();
		JSONObject lastTask = cache.get(lastKey);
		
		return lastTask;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private HashMap parseVariables(JSONObject taskDetails) throws JSONException {
		HashMap map = new HashMap();
		JSONObject variables = taskDetails.getJSONObject("data").getJSONObject("variables");
		Iterator keys = variables.keys();
		while (keys.hasNext()) {
			String variableName = (String) keys.next();
			Object value = variables.get(variableName);
			if (Util.isNotEmpty(String.valueOf(value))) {
				map.put(variableName, value);	
			}
		}
		
		return map;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static HashMap parseModel(JsonObject dataModelObj) throws JSONException {
		HashMap map = new HashMap();
		String types[] = { "inputs", "outputs" };
		
		for (String type : types) {
			JsonObject ios = dataModelObj.getAsJsonObject(type);
			
			Iterator params = ios.entrySet().iterator();
			while (params.hasNext()) {
				Entry<String, JsonElement> entry = (Entry<String, JsonElement>) params.next();
				//LinkedTreeMap linkedMap = (LinkedTreeMap) params.next();
				String variableName = entry.getKey();
				JsonObject param = ios.getAsJsonObject(variableName);
				System.out.println(variableName + " : " + param);
				String variableType = param.get("type").getAsString();
				boolean isList = param.get("isList").getAsBoolean();
				map.put(variableName, variableType);
			}
		}
		
		return map;
	}
	
	public static String ObjectToParamString(Object o, Class<?> clazz) throws GeneralException {
		StringBuilder result = new StringBuilder();
		Method[] methods = clazz.getMethods();
		try {
			for (Method m : methods) {
				if ((!(m.getName().startsWith("get"))) || (m.getName().equals("getClass")))
					continue;
				String field = decapitalize(m.getName().substring(3));

				Object value = m.invoke(o, new Object[0]);

				if (value == null)
					continue;
				
				result.append(field);
				result.append("=");
				result.append(value);
				result.append("&");
			}
		} catch (IllegalArgumentException e) {
			throw new GeneralException(e.getMessage(), e);
		} catch (IllegalAccessException e) {
			throw new GeneralException(e.getMessage(), e);
		} catch (InvocationTargetException e) {
			throw new GeneralException(e.getMessage(), e);
		}

		return result.toString();
	}

	public static String decapitalize(String string){
    	if (string == null || string.length() == 0) {
    		return string;
    	}
    	char c[] = string.toCharArray();
    	c[0] = Character.toLowerCase(c[0]);
    	return new String(c);
	}
	
	public static String callService(String urlParam,String method)
			throws InstantiationException, IllegalAccessException, ClassNotFoundException, AppException,
			SocketTimeoutException {
		String status = "";
		int timeout = TIMEOUT;
		try {
			int responseCode = 0;
			
			URL url = new URL(urlParam);

			if (urlParam.startsWith("https")) {
				// HTTPS
				HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
				String encoded = Base64.getEncoder().encodeToString(("admin:admin").getBytes(StandardCharsets.UTF_8));  //Java 8
				urlConnection.setRequestProperty("Authorization", "Basic "+encoded);
				SSLContext ctx = SSLContext.getInstance("TLS");

				// TODO fixing connection tls
				ctx.init(null, new TrustManager[] { new AlwaysTrustManager() }, null);
				SSLSocketFactory factory = ctx.getSocketFactory();

				urlConnection.setConnectTimeout(timeout);
				urlConnection.setReadTimeout(timeout);
				urlConnection.setSSLSocketFactory(factory);
				urlConnection.setHostnameVerifier(new TrustingHostnameVerifier());

				urlConnection.setRequestMethod(method);

				responseCode = urlConnection.getResponseCode();

				BufferedReader responseBuffer = new BufferedReader(
						new InputStreamReader((urlConnection.getInputStream())));
				
				if (responseCode == HttpURLConnection.HTTP_OK) {
					JsonElement json = new JsonParser().parse(responseBuffer);
					status = gson.toJson(json);
				}
				urlConnection.disconnect();
				responseBuffer.close();
			} else {
				// HTTP
				HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
				urlConnection.setConnectTimeout(timeout);
				urlConnection.setReadTimeout(timeout);
				urlConnection.setRequestMethod(method);

				responseCode = urlConnection.getResponseCode();

				BufferedReader responseBuffer = new BufferedReader(
						new InputStreamReader((urlConnection.getInputStream())));

				if (responseCode == HttpURLConnection.HTTP_OK) {
					JsonElement json = new JsonParser().parse(responseBuffer);
					status = gson.toJson(json);
				}
				urlConnection.disconnect();
				responseBuffer.close();
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
			throw new AppException("error request", e);
		} catch (IOException e) {
			if (e instanceof SocketTimeoutException) {
				throw new SocketTimeoutException(e.getMessage());
			}
			e.printStackTrace();
			throw new AppException("error request", e);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			throw new AppException("error request", e);
		} catch (KeyManagementException e) {
			e.printStackTrace();
			throw new AppException("error request", e);
		}

		return status;

	}
	
	private static final class TrustingHostnameVerifier implements HostnameVerifier {
		public boolean verify(String hostname, SSLSession session) {
			return true;
		}
	}

	private static class AlwaysTrustManager implements X509TrustManager {
		public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException { }
		public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException { }
		public X509Certificate[] getAcceptedIssuers() { return null; }
	}

	private static final String DEFAULT_DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";
	
	public static Timestamp stringToTimestamp(String dateStr){
		return stringToTimestamp(dateStr, DEFAULT_DATETIME_FORMAT);
	}
	
	public static Timestamp stringToTimestamp(String dateStr, String dateFormat){
		Timestamp time = null;
		try{
			SimpleDateFormat format = new SimpleDateFormat(dateFormat);
			Date parsedDate = format.parse(dateStr);
			time = new Timestamp(parsedDate.getTime());
		}catch(Exception e){//this generic but you can control another types of exception
			e.printStackTrace(); 
		}
		return time;
	}
	
	public static String timestampToString(Timestamp timestamp){
		return timestampToString(timestamp, DEFAULT_DATETIME_FORMAT);
	}
	
	public static String dateToString(Date date){
		return timestampToString(new Timestamp(date.getTime()), DEFAULT_DATETIME_FORMAT);
	}
	  
	public static String dateToString(java.util.Date objDate, String Pattern)
	{
		SimpleDateFormat sdt = new SimpleDateFormat(Pattern);
		String strDate = sdt.format(objDate);
		return strDate;
	}

	public static Date stringToDate(String date) {
		SimpleDateFormat sdt = new SimpleDateFormat(DEFAULT_DATETIME_FORMAT);
		Date result = null;
		try {
			result = sdt.parse(date);
		} catch (Exception e) {
			result = null;
		}
		return result;
	}

	public static Date stringToDate(String date, String dateFormat) {
		SimpleDateFormat sdt = new SimpleDateFormat(dateFormat);
		Date result = null;
		try {
			result = sdt.parse(date);
		} catch (Exception e) {
			result = null;
		}
		return result;
	}

	public static String timestampToString(Timestamp timestamp, String dateFormat){
		String timeStr = null;
		try{
			SimpleDateFormat format = new SimpleDateFormat(dateFormat);
			timeStr = format.format(timestamp);
		}catch(Exception e){//this generic but you can control another types of exception
			e.printStackTrace(); 
		}
		return timeStr;
	}
	
}
