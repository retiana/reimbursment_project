package id.co.nds.vertx.model;

import org.json.JSONObject;

public class RequestBPMParam {
	private String action;
	private String bpdId;
	private String processAppId;
	private String parts;
	private boolean toMe;
	private JSONObject params;
	private String taskId;
	private String fields;
	
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getBpdId() {
		return bpdId;
	}
	public void setBpdId(String bpdId) {
		this.bpdId = bpdId;
	}
	public String getProcessAppId() {
		return processAppId;
	}
	public void setProcessAppId(String processAppId) {
		this.processAppId = processAppId;
	}
	public String getParts() {
		return parts;
	}
	public void setParts(String parts) {
		this.parts = parts;
	}
	public boolean isToMe() {
		return toMe;
	}
	public void setToMe(boolean toMe) {
		this.toMe = toMe;
	}
	public JSONObject getParams() {
		return params;
	}
	public void setParams(JSONObject params) {
		this.params = params;
	}
	public String getFields() {
		return fields;
	}
	public void setFields(String fields) {
		this.fields = fields;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}	
}

