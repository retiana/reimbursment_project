package id.co.nds.vertx.model;

public class LoginModel {
	private String name;
	private String password;
	
	public String getNama() {
		return name;
	}
	public void setNama(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	
}
