package id.co.nds.vertx;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.jdbc.JDBCClient;
import io.vertx.ext.sql.SQLConnection;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;

public class PSQLVerticle extends AbstractVerticle {

	@Override
	public void start() {
		final JDBCClient jdbc = JDBCClient.createNonShared(vertx,
				new JsonObject().put("url", "jdbc:postgresql://localhost:5432/customer").put("user", "postgres")
						.put("password", "reti").put("driver_class", "org.postgresql.Driver"));

		final Router router = Router.router(vertx);

		router.route().handler(BodyHandler.create());

		router.route("/sales*").handler(ctx -> jdbc.getConnection(res -> {
			if (res.failed()) {
				ctx.fail(res.cause());
			} else {
				SQLConnection conn = res.result();
				System.out.println("connection success");
				System.out.println(conn.toString());
				// save the connection on the context
				ctx.put("conn", conn);
				System.out.println("connection successfully put to contex");
				SQLConnection conn2 = ctx.get("conn");
				System.out.println("===? check connection : " + conn2.toString());
				ctx.addHeadersEndHandler(done -> conn.close(close -> {
					if (close.failed()) {

					} else {

					}
				}));

				ctx.next();
			}
		})).failureHandler(routingContext -> {
			SQLConnection conn = routingContext.get("conn");
			if (conn != null) {
				conn.close(v -> {
				});
			}
		});
		router.get("/sales/:id").handler(ctx -> {
			String id = ctx.request().getParam("id");
			HttpServerResponse response = ctx.response();
			if (id == null) {
				ctx.fail(400);
				return;
			}

			SQLConnection conn = ctx.get("conn");

			conn.queryWithParams("SELECT id FROM ms_customer WHERE id->>'id' = ?", new JsonArray().add(id), query -> {
				if (query.succeeded()) {
					response.putHeader("content-type", "application/json")
							.end(query.result().getResults().get(0).getString(0));
					for (int i = 0; i < query.result().getResults().size(); i++) {
						System.out.println(query.result().getResults().get(i).getString(i));
					}
				}
				if (query.failed()) {
					ctx.fail(query.cause());
					return;
				}

				if (query.result().getNumRows() == 0) {
					response.setStatusCode(404).end();
				}

			});
		});

		router.post("/sales/add").handler(ctx -> {
			HttpServerResponse response = ctx.response();
			SQLConnection conn = ctx.get("conn");
			System.out.println("conn :" + conn.toString());
			System.out.println("data : " + ctx.getBodyAsString());

			JsonObject jo = ctx.getBodyAsJson();
			System.out.println("data Json : ");
			System.out.println("id : " + jo.getString("id"));
			System.out.println("no_induk : " + jo.getString("no_induk"));
			System.out.println("nama : " + jo.getString("nama"));
			System.out.println("email : " + jo.getString("email"));

			String sql = "INSERT INTO ms_customer (id,no_induk,nama,email) VALUES (?,?,?,?)";
			JsonArray params = new JsonArray();
			params.add(jo.getString("id"));
			params.add(jo.getString("no_induk"));
			params.add(jo.getString("nama"));
			params.add(jo.getString("email"));

			conn.updateWithParams(sql, params, query -> {
				if (query.succeeded()) {

					response.setStatusCode(200).end("berhasill insert data");

				} else if (query.failed()) {

					ctx.fail(query.cause());
					System.out.println("ini error nya : " + query.cause());
					response.setStatusCode(201).end();
					response.setStatusMessage(query.cause().getMessage());
				}
				return;

			});
		});

		router.post("/sales/update").handler(ctx -> {
			HttpServerResponse response = ctx.response();
			SQLConnection conn = ctx.get("conn");
			System.out.println("conn :" + conn.toString());
			System.out.println("data : " + ctx.getBodyAsString());
			String nama = "reti";
			JsonObject jo = ctx.getBodyAsJson();
			String update = "UPDATE ms_customer SET no_induk = ?, nama = ?, email = ? WHERE id = ?";
			JsonArray params = new JsonArray();
			params.add(jo.getString("no_induk"));
			params.add(jo.getString("nama"));
			params.add(jo.getString("email"));
			params.add(jo.getString("id"));
			conn.updateWithParams(update, params, res -> {

				if (res.succeeded()) {

					response.setStatusCode(200).end("berhasil update data");

				} else if (res.failed()) {

					ctx.fail(res.cause());
					System.out.println("ini error nya : " + res.cause());
					response.setStatusCode(201).end();
					response.setStatusMessage(res.cause().getMessage());
				}
			});
		});

				router.post("/sales/update").handler(ctx -> {
					HttpServerResponse response = ctx.response();
					SQLConnection conn = ctx.get("conn");
					System.out.println("conn :" + conn.toString());
					System.out.println("data : " + ctx.getBodyAsString());
					String nama = "reti";
					JsonObject jo = ctx.getBodyAsJson();
					String update = "DELETE from ms_customer where id=? ";
					JsonArray params = new JsonArray();
					params.add(jo.getString("id"));
					conn.updateWithParams(update, params, res -> {

						if (res.succeeded()) {

							response.setStatusCode(200).end("berhasil delete data");

						} else if (res.failed()) {

							ctx.fail(res.cause());
							System.out.println("ini error nya : " + res.cause());
							response.setStatusCode(201).end();
							response.setStatusMessage(res.cause().getMessage());
						}
					});
				});
				
		vertx.createHttpServer().requestHandler(router::accept).listen(
				config().getInteger("http.port", 9990));
	}
}
