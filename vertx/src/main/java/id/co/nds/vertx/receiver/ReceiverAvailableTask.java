package id.co.nds.vertx.receiver;

import org.apache.log4j.Logger;

import io.vertx.core.AbstractVerticle;

public class ReceiverAvailableTask extends AbstractVerticle {
	private static final Logger logger = Logger.getLogger(ReceiverLogin.class);
	
	@Override
    public void start() {
        vertx.eventBus().consumer("api.response.task", message -> {
            
            
            message.reply("sudah berhasil dikirim");
        });
    }
}
