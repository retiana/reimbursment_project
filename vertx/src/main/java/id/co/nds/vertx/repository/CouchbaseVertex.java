package id.co.nds.vertx.repository;
import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.CouchbaseCluster;
import com.couchbase.client.java.document.JsonDocument;
import com.couchbase.client.java.document.json.JsonObject;
import com.couchbase.client.java.env.CouchbaseEnvironment;
import com.couchbase.client.java.env.DefaultCouchbaseEnvironment;
import com.couchbase.client.java.query.N1qlQueryResult;

import id.co.nds.vertx.model.ReimburseData;
import io.vertx.core.*;
import io.vertx.core.http.*;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import org.codehaus.jackson.map.ObjectMapper;
import com.couchbase.client.java.query.N1qlQuery;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

///------------------crud operation by using vertx with couchbase-------------------

public class CouchbaseVertex extends AbstractVerticle {

	Bucket bucket;

	public static void main(String[] args) {

		Launcher.executeCommand("run", CouchbaseVertex.class.getName());

	}

	private static ObjectMapper objectMapper = new ObjectMapper();

	@Override
	public void start(Future<Void> fut) throws IOException {

		// createSomeData();
		CouchbaseEnvironment env = DefaultCouchbaseEnvironment.builder()
				// this set the IO socket timeout globally, to 45s
				.socketConnectTimeout((int) TimeUnit.SECONDS.toMillis(10000))
				// this sets the connection timeout for openBucket calls globally (unless a
				// particular call provides its own timeout)
				.connectTimeout(TimeUnit.SECONDS.toMillis(12000)).build();
		Cluster cluster = CouchbaseCluster.create(env, "http://127.0.0.1:8091");
		cluster.authenticate("Administrator", "123456");
		bucket = cluster.openBucket("mahasiswa");
		System.out.println("bucket open........");
		JsonObject products = JsonObject.create();

		Router router = Router.router(vertx);

		ObjectMapper objectMapper = new ObjectMapper();

		router.route("/").handler(routingContext -> {
			HttpServerResponse response = routingContext.response();
			response.putHeader("content-type", "text/html").end("<h1>Hello from my first Vert.x 3 application</h1>");
		});

		router.route("/assets*").handler(BodyHandler.create());

		router.post("/assets/add").handler(this::addOne1);
		router.get("/assets/getAll").handler(this::getAll);

		router.get("/assets/find/:nim").handler(this::finone);

		router.delete("/assets/delete/:nim").handler(this::deleteone);

		router.post("/assets/update/:nim").handler(this::update1);

		vertx.createHttpServer().requestHandler(router::accept).listen(
				// Retrieve the port from the configuration,
				// default to 8080.
				8900, result -> {
					if (result.succeeded()) {
						fut.complete();
					} else {
						fut.fail(result.cause());
					}
				});
	}

	private void addOne1(RoutingContext routingContext) {
		String s1 = routingContext.getBodyAsString();
		System.out.println(s1);
		ReimburseData reimburseData = null;
		try {
			reimburseData = new ObjectMapper().readValue(s1, ReimburseData.class);

		} catch (Exception e) {
			e.getCause();
		}

		System.out.println("NEW VALUE........" + s1);

		final JsonObject jsonObject = JsonObject.fromJson(s1);
		JsonDocument jsonDocument = JsonDocument.create(String.valueOf(reimburseData.getLoginData().get(0).getNama()), jsonObject);
		bucket.insert(jsonDocument);
		routingContext.response().setStatusCode(209).putHeader("content-type", "application/json; charset=utf-8")
				.end(s1);

	}

	private void getAll(RoutingContext routingContext) {
		N1qlQuery qlQuery = N1qlQuery.simple("SELECT * FROM ms_customer");
		System.out.println(qlQuery);
		N1qlQueryResult n1qlQueryResult = bucket.query(qlQuery);
		System.out.println(n1qlQueryResult);
		routingContext.response().setStatusCode(200).putHeader("content-type", "application/json; charset=utf-8")
				.end(n1qlQueryResult.allRows().toString());
	}

	private void finone(RoutingContext routingContext) {
		String id1 = routingContext.request().getParam("id");
		System.out.println(id1);
		JsonDocument jsonDocument = bucket.get(id1);
		JsonObject jsonObject = jsonDocument.content();
		System.out.println(jsonObject);
		routingContext.response().setStatusCode(201).putHeader("content-type", "application/json; charset=utf-8")
				.end(jsonObject.toString());
	}

	private void deleteone(RoutingContext routingContext) {
		String s1 = routingContext.request().getParam("nim");
		System.out.println(s1);
		JsonDocument jsonDocument = bucket.remove(s1);
		System.out.println(jsonDocument.content());
		routingContext.response().setStatusCode(200).putHeader("content-type", "application/json; charset=utf-8")
				.end(jsonDocument.content().toString());
	}

	private void update1(RoutingContext routingContext) {
		String s1 = routingContext.getBodyAsString();
		System.out.println("value of passing body ...............");
		System.out.println("VALUE..............." + s1);
	//	Mahasiswa student = null;
		try {
	//		student = new ObjectMapper().readValue(s1, Mahasiswa.class);

		} catch (Exception e) {
			e.getCause();
		}
	//	System.out.println(student.getNim());

		JsonObject jsonObject = JsonObject.fromJson(s1);

	//	JsonDocument jsonDocument = JsonDocument.create(String.valueOf(student.getNim()), jsonObject);
	//	bucket.replace(jsonDocument);

		//routingContext.response().setStatusCode(201)

	//			.putHeader("content-type", "application/json; charset=utf-8").end(jsonDocument.content().toString());
	}

}
