package id.co.nds.vertx.model;

import java.util.ArrayList;

public class InputRequest {
	private int id;
	private String name;
	private String department;
	private ArrayList<ReimburseModel> reimburse;
	private String reimburseType;

		public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getReimburseType() {
		return reimburseType;
	}

	public void setReimburseType(String reimburseType) {
		this.reimburseType = reimburseType;
	}

	public ArrayList<ReimburseModel> getReimburse() {
		return reimburse;
	}

	public void setReimburse(ArrayList<ReimburseModel> reimburse) {
		this.reimburse = reimburse;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
