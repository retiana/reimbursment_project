package id.co.nds.vertx;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import org.apache.log4j.Logger;

import id.co.nds.vertx.receiver.ReceiverAvailableTask;
import id.co.nds.vertx.receiver.ReceiverLogin;
import id.co.nds.vertx.receiver.ReceiverRequest;
import id.co.nds.vertx.repository.RestApiVerticle;

public class MainVerticle extends AbstractVerticle {

	private static final Logger logger = Logger.getLogger(MainVerticle.class);

	public static void main(String[] args) throws InterruptedException {
		Vertx vertx = Vertx.vertx();
		vertx.deployVerticle(new MainVerticle());
	}

	@Override
	public void start(Future<Void> startFuture) throws Exception {
		logger.info("\n\n\n\n Deployed main module " + startFuture + Thread.currentThread().getName());
		
		vertx.deployVerticle(new RestApiVerticle());
		vertx.deployVerticle(new ReceiverLogin());
		vertx.deployVerticle(new ReceiverRequest());
		vertx.deployVerticle(new ReceiverAvailableTask());
	}

	@Override
	public void stop() throws Exception {
		logger.info("\n\n\n\n Successfully stopping verticle.");
	}
}

