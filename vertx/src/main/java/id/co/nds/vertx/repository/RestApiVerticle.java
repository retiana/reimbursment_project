package id.co.nds.vertx.repository;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import org.apache.log4j.Logger;
import com.google.gson.Gson;
import id.co.nds.vertx.model.AvailableTask;
import id.co.nds.vertx.model.InputRequest;
import id.co.nds.vertx.model.LoginModel;
import id.co.nds.vertx.model.StandardResponse;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.Json;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CorsHandler;

public class RestApiVerticle extends AbstractVerticle {

	private static final Logger logger = Logger.getLogger(RestApiVerticle.class);
	private ArrayList<InputRequest> requestList = new ArrayList<>();
	static Gson gson = new Gson();

	@Override
	public void start(Future<Void> startFuture) {
		logger.info("Deployed verticle [" + this.getClass().getName() + "]");

		Router router = Router.router(vertx);
		Set<String> allowedHeaders = new HashSet<>();
		allowedHeaders.add("x-requested-with");
		allowedHeaders.add("Access-Control-Allow-Origin");
		allowedHeaders.add("origin");
		allowedHeaders.add("Content-Type");
		allowedHeaders.add("accept");
		allowedHeaders.add("X-PINGARUNER");

		Set<HttpMethod> allowedMethods = new HashSet<>();
		allowedMethods.add(HttpMethod.GET);
		allowedMethods.add(HttpMethod.POST);
		allowedMethods.add(HttpMethod.OPTIONS);

		allowedMethods.add(HttpMethod.DELETE);
		allowedMethods.add(HttpMethod.PATCH);
		allowedMethods.add(HttpMethod.PUT);

		router.route().handler(CorsHandler.create("*").allowedHeaders(allowedHeaders).allowedMethods(allowedMethods));

		router.route("/coba*").handler(BodyHandler.create());
		router.post("/coba/login").handler(this::login);
		router.post("/coba/request").handler(this::getRequest);
		router.post("/coba/task").handler(this::getAvailableTask);

		vertx.createHttpServer().requestHandler(router::accept).listen(
				// Retrieve the port from the configuration,
				// default to 8080.
				config().getInteger("http.port", 9090), result -> {
					if (result.succeeded()) {
						startFuture.complete();
					} else {
						startFuture.fail(result.cause());
					}
				});
	}

	private void login(RoutingContext route) {
		System.out.println("TEST>>>" + route.getBodyAsString());
		vertx.eventBus().send("api.login.rembes", route.getBodyAsString(), response -> {
			boolean statusLogin;
			StandardResponse responseLogin = new StandardResponse();
			if (response.succeeded()) {
				statusLogin = true;
				System.out.println(route.getBodyAsString());
				LoginModel loginObject = gson.fromJson(route.getBodyAsString(), LoginModel.class);
				responseLogin.setStatusCode(200);
				responseLogin.setStatusDesc("login success");
				route.response().setStatusCode(responseLogin.getStatusCode())
						.setStatusMessage(responseLogin.getStatusDesc()).setStatusMessage(responseLogin.getStatusDesc())
						.end(responseLogin.getStatusDesc());
				System.out.println(route.response().getStatusCode());
				System.out.println(route.response().getStatusMessage());
			} else {
				logger.error("Can't Login", response.cause());
				statusLogin = false;
				responseLogin.setStatusCode(500);
				responseLogin.setStatusDesc("login unsuccessful");
				route.response().setStatusCode(responseLogin.getStatusCode())
						.setStatusMessage(responseLogin.getStatusDesc()).setStatusMessage(responseLogin.getStatusDesc())
						.end(responseLogin.getStatusDesc());
				System.out.println(route.response().getStatusCode());
				System.out.println(route.response().getStatusMessage());
			}
		});
	}

	private void getRequest(RoutingContext route) {
		System.out.println("TEST>>>" + route.getBodyAsString());
		vertx.eventBus().send("api.request.rembes", route.getBodyAsString(), response -> {
			StandardResponse responseRequest = new StandardResponse();
			if (response.succeeded()) {
				// InputRequest loginObject = gson.fromJson(route.getBodyAsString(),
				// InputRequest.class);
				InputRequest loginObject = Json.decodeValue(route.getBodyAsString(), InputRequest.class);
				responseRequest.setStatusCode(200);
				responseRequest.setStatusDesc("request success");
				route.response().setStatusCode(responseRequest.getStatusCode())
						.setStatusMessage(responseRequest.getStatusDesc()).setStatusMessage(responseRequest.getStatusDesc())
						.end(responseRequest.getStatusDesc());
				System.out.println(route.response().getStatusCode());
				System.out.println(route.response().getStatusMessage());
			} else {
				logger.error("Can't send request", response.cause());
				responseRequest.setStatusCode(500);
				responseRequest.setStatusDesc("request unsuccessful");
				route.response().setStatusCode(responseRequest.getStatusCode())
						.setStatusMessage(responseRequest.getStatusDesc()).setStatusMessage(responseRequest.getStatusDesc())
						.end(responseRequest.getStatusDesc());
				System.out.println(route.response().getStatusCode());
				System.out.println(route.response().getStatusMessage());
			}
		});
	}

	private void getAvailableTask(RoutingContext route) {
		String a = "{\"taskName\":\"input\",\"status\":\"pending\",\"taskId\" :\"21\",\"processId\":\"1\"}";
		
		vertx.eventBus().send("api.response.task", a, response -> {
			StandardResponse responseTask = new StandardResponse();
			if (response.succeeded()) {
				// InputRequest loginObject = gson.fromJson(route.getBodyAsString(),
				// InputRequest.class);
				AvailableTask loginObject = Json.decodeValue(a, AvailableTask.class);
				responseTask.setStatusCode(200);
				responseTask.setStatusDesc("available task now");
				route.response().setStatusCode(responseTask.getStatusCode())
						.setStatusMessage(responseTask.getStatusDesc()).setStatusMessage(responseTask.getStatusDesc())
						.end(a);
				System.out.println(route.response().getStatusCode());
				System.out.println(route.response().getStatusMessage());
			} else {
				logger.error("Can't get the available task", response.cause());
				responseTask.setStatusCode(500);
				responseTask.setStatusDesc("can't get the available task");
				route.response().setStatusCode(responseTask.getStatusCode())
						.setStatusMessage(responseTask.getStatusDesc()).setStatusMessage(responseTask.getStatusDesc())
						.end(responseTask.getStatusDesc());
				System.out.println(route.response().getStatusCode());
				System.out.println(route.response().getStatusMessage());
			}
		});
	}
}
