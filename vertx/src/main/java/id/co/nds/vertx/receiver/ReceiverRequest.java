package id.co.nds.vertx.receiver;

import org.apache.log4j.Logger;

import io.vertx.core.AbstractVerticle;

public class ReceiverRequest extends AbstractVerticle {
	
	private static final Logger logger = Logger.getLogger(ReceiverLogin.class);
	
	@Override
    public void start() {
        vertx.eventBus().consumer("api.request.rembes", message -> {
                       
            message.reply("sudah berhasil dikirim");
        });
    }

}
